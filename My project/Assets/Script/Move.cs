using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Move : MonoBehaviour
{
    public TextMeshProUGUI textoVida;
    public int coletado;
    public int vida;
    private int vidaMax;
    private Vector2 checkpoint;
    public float velocidade;
    public float velPulo;
    private Rigidbody2D rb;
    public bool podePular;
    public float inputDirection;
    public bool isDirectionRight;
    public Animator anim;
    
    // Start is called before the first frame update
    void Start()
    {
        checkpoint = transform.position;
        vidaMax = vida;
        textoVida.text = vida.ToString();
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Flip();
        GetInput();
        animController();
       
        if(Input.GetKeyDown(KeyCode.Space) && podePular == true)
        {
            podePular = false;
            rb.velocity = new Vector2(rb.velocity.x, velPulo);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("chao"))
        {
            podePular = true;
        }
        if (collision.gameObject.CompareTag("Inimigo"))
        {
            vida -= collision.gameObject.GetComponent<inimigo>().Dano;
            textoVida.text = vida.ToString();
            if(vida <= 0)
            {
                //Destroy(gameObject);
                Respawn();
                textoVida.text = vida.ToString();
            }
        }
    }   

    private void FixedUpdate()
    {
        MoveLogic();
    }

    void GetInput()
    {
        inputDirection = Input.GetAxis("Horizontal");
    }

    void MoveLogic()
    {
        rb.velocity = new Vector2(inputDirection * velocidade, rb.velocity.y);
    }

    void Flip()
    {
        if(isDirectionRight && inputDirection > 0)
        {
            FlipLogic();
        }
        if(!isDirectionRight && inputDirection < 0)
        {
            FlipLogic();
        }
    }

    void FlipLogic()
    {
        isDirectionRight = !isDirectionRight;
        transform.Rotate(0f, 180.0f, 0f);
    }

    void animController()
    {
        anim.SetFloat ("Horizontal", rb.velocity.x);
        anim.SetFloat ("Vertical", rb.velocity.y);
        anim.SetBool ("PodePular", podePular);
    }
    void Respawn()
    {
        transform.position = checkpoint;
        vida = vidaMax;
    }
}
