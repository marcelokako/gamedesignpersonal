using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inimigo : MonoBehaviour
{
    private Rigidbody2D rb;
    public float velocidade;
    public int Dano; //No Inimigo

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        StartCoroutine(TempoDePatrulha());
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(velocidade, rb.velocity.y);
        //Vector2.MoveTowards(gameObject.transform.position, sadas.transform.position, 0,1f);
    }

    IEnumerator TempoDePatrulha()
    {
        yield return new WaitForSeconds(2f);
        velocidade = velocidade * -1;
        StartCoroutine(TempoDePatrulha());
    }
}
